///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//   ���������������� ������ ������������� ���������� TBComPort. ����������  //
// ���������� �������� ������ ���������� �����������. ����� CR+LF ��������-  //
// ��������� ����� ��������������� ���������� �������� ������ ��� ������     //
// ������� �������� ������� � Memo1.                                         //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

unit Main;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, BCPort, XPMan, bsSkinCtrls, bsSkinBoxCtrls,
  bsSkinData, BusinessSkinForm, Mask,dateutils,math;

type
  TMainForm = class(TForm)
    BComPort1: TBComPort;
    bsBusinessSkinForm1: TbsBusinessSkinForm;
    bsSkinData1: TbsSkinData;
    bsResourceStrData1: TbsResourceStrData;
    bsCompressedSkinList1: TbsCompressedSkinList;
    Panel: TbsSkinPanel;
    bsSkinStdLabel1: TbsSkinStdLabel;
    cbPort: TbsSkinComboBox;
    btnConnect: TbsSkinButton;
    btnDisconnect: TbsSkinButton;
    bsSkinButton2: TbsSkinButton;
    bsSkinGroupBox1: TbsSkinGroupBox;
    bsSkinTimeEdit1: TbsSkinTimeEdit;
    bsSkinSpinEdit1: TbsSkinSpinEdit;
    bsSkinComboBox1: TbsSkinComboBox;
    bsSkinGroupBox2: TbsSkinGroupBox;
    bsSkinTimeEdit2: TbsSkinTimeEdit;
    bsSkinSpinEdit2: TbsSkinSpinEdit;
    bsSkinComboBox2: TbsSkinComboBox;
    bsSkinButton4: TbsSkinButton;
    bsSkinButton1: TbsSkinButton;
    bsSkinGroupBox3: TbsSkinGroupBox;
    bsSkinSpinEdit3: TbsSkinSpinEdit;
    bsSkinSpinEdit4: TbsSkinSpinEdit;
    PaintPanel: TbsSkinPaintPanel;
    GraphScroll: TbsSkinScrollBar;
    bsSkinSpeedButton1: TbsSkinSpeedButton;
    bsSkinSpeedButton2: TbsSkinSpeedButton;
    bsSkinSpeedButton3: TbsSkinSpeedButton;
    bsSkinSpeedButton4: TbsSkinSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure btnConnectClick(Sender: TObject);
    procedure btnDisconnectClick(Sender: TObject);
    procedure BComPort1RxChar(Sender: TObject; Count: Integer);
    procedure send(s:string);
    procedure Ready;
    procedure bsSkinButton2Click(Sender: TObject);
    procedure synchr;
    procedure bsSkinButton4Click(Sender: TObject);
    procedure bsSkinButton1Click(Sender: TObject);
    procedure PaintPanelPanelPaint(Cnvs: TCanvas; R: TRect);
    procedure GraphScrollChange(Sender: TObject);
    procedure bsSkinSpeedButton2Click(Sender: TObject);
    procedure GraphUpdate;
  private
    // ���������-���������� �����������
     ps,pe,pcrs,ptb:boolean;
     rs_ready:boolean;
     Log:array [0..250] of Byte;
     LogDate:tdate;
     LogTime:tdate;
     LogStart:integer;
     rs_Buff:array [0..512] of byte;
     rs_state :integer;
     rs_buff_pos:integer;
     rs_synh:integer;

     //graph++++++++++++++++++++++++++++++
     VScale,HScale:integer;
  end;

var
  MainForm: TMainForm;

implementation

{$R *.DFM}

procedure TMainForm.FormCreate(Sender: TObject);
begin
  EnumComPorts(cbPort.Items);
  cbPort.ItemIndex := 0;
  rs_ready:=true;
  rs_state:=0;
  rs_synh:=0;
  ps:=false;
  pe:=false;
  pcrs:=false;
  ptb:=false;

  //=++++
  VScale:=1;
  HScale:=11;
  GraphUpdate;
end;

procedure TMainForm.btnConnectClick(Sender: TObject);
begin
  BComPort1.Port := cbPort.Text;
  BComPort1.BaudRate := TBaudRate(br9600);
  BComPort1.Open;

  btnConnect.Enabled := False;
  cbPort.Enabled := False;
  btnDisconnect.Enabled := True;
  rs_ready:=true;
  send(#4);


end;

procedure TMainForm.btnDisconnectClick(Sender: TObject);
begin
  BComPort1.Close;
  btnConnect.Enabled := True;
  cbPort.Enabled := True;
  btnDisconnect.Enabled := False;

end;

procedure TMainForm.send(s:string);
begin
 if not rs_ready then exit;
  if BComPort1.Connected then BComPort1.WriteStr(#255+s+#254);
  if s[1]<>#3 then rs_ready:=false;
end;

procedure TMainForm.BComPort1RxChar(Sender: TObject; Count: Integer);
var
  s:string;
  n:integer;
begin

 while BComPort1.InBufCount>0 do begin
  BComPort1.ReadStr(S, Count);
  for n:=1 to length(s) do
   begin
    if s[n]=#255 then begin rs_buff_pos:=0; rs_state:=1; end;
    if (s[n]=#254) and (rs_state=1) then begin rs_state:=2; ready; end;
    if (s[n]<#253) and (rs_state=1) then begin rs_buff[rs_buff_pos]:=ord(s[n]); inc(rs_buff_pos); end;

   end;
  end;
end;


procedure TMainForm.Ready;
var n:integer;
begin
if rs_buff[0]=0 then rs_ready:=true;

if rs_buff[0]=2 then
 try

    LogDate:=Date;
    if rs_buff[240]<32 then LogDate:=RecodeDay(LogDate,rs_buff[240]);
    if rs_buff[241]<13 then LogDate:=RecodeMonth(LogDate,rs_buff[241]);
    if rs_buff[242]<23 then LogTime:=RecodeHour(LogTime,rs_buff[242]);
    if rs_buff[243]<61 then LogTime:=RecodeMinute(LogTime,rs_buff[243]);
    LogStart:=rs_buff[244];
   for n:=0 to 238 do
   begin
    Log[n]:=rs_buff[n+1];
   end;
 finally
     rs_ready:=true;
    GraphUpdate;
    synchr;
 end;

if rs_buff[0]=4 then
 begin
    bsSkinSpinEdit1.Value:=rs_buff[3];
    bsSkinSpinEdit2.Value:=rs_buff[7];
    bsSkinSpinEdit3.Value:=rs_buff[9];
    bsSkinSpinEdit4.Value:=rs_buff[10];
    bsSkinComboBox1.ItemIndex:=rs_buff[4];
    bsSkinComboBox2.ItemIndex:=rs_buff[8];
    bsSkinTimeEdit1.EncodeTime(rs_buff[1],rs_buff[2],0,0);
    bsSkinTimeEdit2.EncodeTime(rs_buff[5],rs_buff[6],0,0);

     rs_ready:=true;
    send(#2);
 end;
rs_state:=0;
end;

procedure TMainForm.bsSkinButton2Click(Sender: TObject);
begin
 send(#7);
end;

procedure TMainForm.synchr;
begin
   send(#5+char(Dayof(date))+char(Monthof(date))+char(Yearof(date))+
 char(DayofTheWeek(date))+char(Hourof(time))+char(Minuteof(time))+char(Secondof(time)));
end;

procedure TMainForm.bsSkinButton4Click(Sender: TObject);
begin
  send(#4);
end;

procedure TMainForm.bsSkinButton1Click(Sender: TObject);
var Hour,Min,Sec,MSec:word;
    s:string;
begin
  bsSkinTimeEdit1.DecodeTime(Hour,Min,Sec,MSec);
  s:=#6+char(Hour)+char(Min)+char(strtoint(bsSkinSpinEdit1.Text))+
   char(bsSkinComboBox1.ItemIndex);
  bsSkinTimeEdit2.DecodeTime(Hour,Min,Sec,MSec);

   s:=s+char(Hour)+char(Min)+char(strtoint(bsSkinSpinEdit2.Text))+
   char(bsSkinComboBox2.ItemIndex)+char(strtoint(bsSkinSpinEdit3.Text))+
    char(strtoint(bsSkinSpinEdit4.Text));
   send(s);
end;

procedure TMainForm.PaintPanelPanelPaint(Cnvs: TCanvas; R: TRect);
var  n:integer;
  l,h,c,p,at,ah:integer;
  Ime:ttime;
  procedure incc(var i:integer);
  begin
    i:=i+1;
    if i>238 then i:=0;
  end;
  function GDe(i:integer):integer;
  begin
    result:=i-1;
    if result<0 then result:=238;
  end;
  function GIe(i:integer):integer;
  begin
    result:=i+1;
    if result>238 then result:=0;
  end;
  procedure Text(x,y:integer;s:string);
  var OldBkMode : integer;
  begin
   OldBkMode := SetBkMode(Cnvs.Handle, Transparent);
   Cnvs.TextOut(x,y,s);
   SetBkMode(Cnvs.Handle, OldBkMode);
  end;
begin
  p:=GraphScroll.Position;
  l:=10;
 // w:=r.Right-l*2;
  h:=(r.Bottom-r.Top)-((r.Bottom-r.Top) div 5);


  
  Cnvs.Pen.Color:=clwhite;
  Cnvs.Pen.Width:=2;
  c:=LogStart;
  incc(c);
  Ime:=Logtime-strtotime('11:54:00');
  for n:=1 to 239 do
   begin
     Cnvs.MoveTo(l+round(n*HScale-p),h-Log[GDe(c)]*VScale);
     Cnvs.LineTo(l+round(n*HScale-p),h-Log[c]*VScale);
     Cnvs.LineTo(l+round((n+1)*HScale-p),h-Log[c]*VScale);
     if c=LogStart then
      begin
        cnvs.Font.Color:=clred;
        Cnvs.Pen.Color:=clred;
        Cnvs.Pen.Width:=1;
        at:=min(h-Log[c]*VScale,h-Log[c]*VScale)-(h div 15);
        ah:=h+cnvs.TextHeight('A')*3+2;
        Cnvs.MoveTo(l+round(n*HScale-p+HScale/2)+1,at);
        Cnvs.LineTo(l+round(n*HScale-p+HScale/2)+1,ah);
        ah:=ah-cnvs.TextHeight('A');
        Text(l+round(n*HScale-p+HScale/2)-3-cnvs.TextWidth(datetostr(logdate)),ah,datetostr(logdate));
        ah:=ah-cnvs.TextHeight('A');
        Text(l+round(n*HScale-p+HScale/2)-3-cnvs.TextWidth(datetostr(logdate)),ah,timetostr(logtime));
        cnvs.Font.Color:=paintpanel.FontColor;
        Cnvs.Pen.Color:=clwhite;
        Cnvs.Pen.Width:=2;
      end;
      if minuteof(ime)<3 then
      begin
        cnvs.Font.Color:=clblue;
        Cnvs.Pen.Color:=clblue;
        Cnvs.Pen.Width:=1;
        at:=min(h-Log[c]*VScale,h-Log[c]*VScale)-(h div 15);
        ah:=h+cnvs.TextHeight('A')+2;
        Cnvs.MoveTo(l+round(n*HScale-p+HScale/2)-1,at);
        Cnvs.LineTo(l+round(n*HScale-p+HScale/2)-1,ah);
        ah:=ah-cnvs.TextHeight('A');
        Text(l+round(n*HScale-p+HScale/2)-3-cnvs.TextWidth(datetostr(ime)),ah,timetostr(ime));
        cnvs.Font.Color:=paintpanel.FontColor;
        Cnvs.Pen.Color:=clwhite;
        Cnvs.Pen.Width:=2;
      end;
    Ime:=Ime+strtotime('00:03:00');
    incc(c);
   end;

end;

procedure TMainForm.GraphScrollChange(Sender: TObject);
begin
paintpanel.Repaint;
end;

procedure TMainForm.GraphUpdate;
//var n,sm:integer;
begin
 // sm:=10;
 // for n:=0 to 238 do
 //  if log[n]>sm then sm:=log[n];
 // h:=PaintPanel.Height-(PaintPanel.Height div 2);
 // if sm div h >=1 then HScale:= sm div h +1;
  if (239*HScale-(PaintPanel.Width+20))>0 then begin GraphScroll.Max:=(239*HScale)-PaintPanel.Width+40;
  GraphScroll.Visible:=true; end
   else begin GraphScroll.Max:=1; GraphScroll.Visible:=false; end;
  GraphScroll.Position:=GraphScroll.Max;
  paintpanel.Repaint;
  //caption:=timetostr(time+strtotime('00:06:00'));
end;

procedure TMainForm.bsSkinSpeedButton2Click(Sender: TObject);
begin
 case (sender as tbsSkinSpeedButton).Tag of
  1:dec(HScale);
  2:inc(HScale);
  3:inc(VScale);
  4:dec(VScale);
 end;
 GraphUpdate;
end;

end.
