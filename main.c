#include "main.h"
#include "flex_lcdm.c"
#include "ds1307.C"

   struct TTime{
	    int8 hrs;
	    int8 min;
	    int8 sec;
	  };  
   struct TDate{
	    int8 year;
	    int8 mth;
	    int8 day;
	    int8 dow;
	    
	  }; 
   struct TAlarm{
	    int8 hrs;
	    int8 min;
	    int8 win;
	    int8 bweek;
	  };  
   struct TRecTime{
	    int8 SHrs;
	    int8 FHrs;
	  }; 
   int8 n;
// ADC+++++++++++
   int8 jvh=127;
   int8 jvv=127;
   byte ch=0;
//joystic++++++++
   int8 JTimer=0;
   int8 JTLevelV=0;
   int1 JTLevelH=0;
   int1 but1=0;
   int1 JAct=0;
//menu+++++++++++ 
   int8 menu_mn=0;
   int8 menu_lv=0;
   int8 menu_sb_pos=0;  
   int8 set[3];
   int16 LCD_Ligth_time=10;
   int8 LCD_duty=120;

//rs232++++++++++
   int8 rs_buff[70];
   int8 rs_state;
   int8 rs_buff_pos;
   
//sound++++++++++ 
	int16 sound_time=0; 
	int1  sound_en=1;
	int1  sound_en_end=1;
	int8  pddf=0;
	int8  pddfo=0;
//LCD++++++++++++
	int8 BRG_Mn=30;
	int8 BRG_Mx=200;

//Log***************
  int1 TLog_Rec=0;
  int16 LogTime;
  int8 LogCnt;
  struct TRecTime LogSetting;
//timer++++++++++
  int8 Tmrs[4];	
   
struct TTime Time;    
struct TDate Date;
struct TAlarm Alarm[2];	 

void RecLog();
 
#int_RDA
void RDA_isr(void) 
{int8 data_rs;
data_rs=fgetc(PC);
if (data_rs==255){rs_state=1; rs_buff_pos=0;};
if (data_rs==254 & rs_state==1)rs_state=2;
if (data_rs<253 & rs_state==1)  rs_buff[rs_buff_pos++]=data_rs;	 
}

void SettingWrite()
{
  write_eeprom(244,Alarm[0].hrs);
  write_eeprom(245,Alarm[0].min);
  write_eeprom(246,Alarm[0].win);
  write_eeprom(247,Alarm[0].bweek);	
  write_eeprom(248,Alarm[1].hrs);
  write_eeprom(249,Alarm[1].min);
  write_eeprom(250,Alarm[1].win);
  write_eeprom(251,Alarm[1].bweek);
  write_eeprom(252,LogSetting.SHrs);
  write_eeprom(253,LogSetting.FHrs); 
}
void SettingRead()
{
  Alarm[0].hrs=read_eeprom(244);
  Alarm[0].min=read_eeprom(245);
  Alarm[0].win=read_eeprom(246);
  Alarm[0].bweek=read_eeprom(247);	
  Alarm[1].hrs=read_eeprom(248);
  Alarm[1].min=read_eeprom(249);
  Alarm[1].win=read_eeprom(250);
  Alarm[1].bweek=read_eeprom(251);
  LogSetting.SHrs=read_eeprom(252);
  LogSetting.FHrs=read_eeprom(253);
}
void WDayOut(int8 num)
{
 switch (num)
 {
 case 0:printf(lcd_putc,".......");break;	
 case 1:printf(lcd_putc,"�......");break;
 case 2:printf(lcd_putc,".B.....");break;
 case 3:printf(lcd_putc,"..C....");break;
 case 4:printf(lcd_putc,"...�...");break;
 case 5:printf(lcd_putc,"....�..");break;
 case 6:printf(lcd_putc,".....C.");break;
 case 7:printf(lcd_putc,"......B");break;
 case 8:printf(lcd_putc,"�BC��..");break;
 case 9:printf(lcd_putc,".....CB");break;
 case 10:printf(lcd_putc,"�BC��CB");break;
 }	
}
void DDayOut(int8 num)
{
 switch (num)
 {
 case 1:printf(lcd_putc,"�o�e�e��.");break;
 case 2:printf(lcd_putc,"  B�op���");break;
 case 3:printf(lcd_putc,"    Cpe�a");break;
 case 4:printf(lcd_putc,"  �e��ep�");break;
 case 5:printf(lcd_putc,"  �ǿ���a");break;
 case 6:printf(lcd_putc,"  Cy��o�a");break;
 case 7:printf(lcd_putc,"Boc�pece�.");break;
 }	
}
signed int16 TimeToInt16()
{
 return((int16)time.hrs*60+time.min );		
} 
signed int16 AlarmToInt16(int8 Num)
{
 return((int16)alarm[Num].hrs*60+alarm[Num].min);		
} 
void menu_view()
{
int8 m_item;
m_item=menu_lv*3+menu_mn;
 switch (m_item){
  case 0:printf(lcd_putc,"\r <> %02u:%02u:%02u <>\n", time.hrs,time.min,time.sec);
                     DDayOut(date.dow); printf(lcd_putc," %02u.%02u", date.day,date.mth); break;
  case 1:printf(lcd_putc,"\r�y㸻Ľ��1 %02u:%02u\nO��o: %03u M��.", alarm[0].hrs,alarm[0].min,alarm[0].win); break;
  case 2:printf(lcd_putc,"\r�y㸻Ľ��2 %02u:%02u\nO��o: %03u M��.", alarm[1].hrs,alarm[1].min,alarm[1].win); break;
case 3:printf(lcd_putc,"\rHac�p. Bpe�e��");   switch(menu_sb_pos){
	       				  							case 0:printf(lcd_putc,"\n�ac�: %02u",set[0]); break;
	       				  							case 1:printf(lcd_putc,"\nM��y��: %02u",set[1]); break;
	       				  							case 2:printf(lcd_putc,"\n�e�� He�.: %1u",set[2]); break;
	       				  							}; break;  
	       				  							
  case 6:printf(lcd_putc,"\rHac�po��a �a�� ");   switch(menu_sb_pos){
	       				  							case 0:printf(lcd_putc,"\n�e��: %02u",set[0]); break;
	       				  							case 1:printf(lcd_putc,"\nMec��: %02u",set[1]); break;
	       				  							case 2:printf(lcd_putc,"\n�o�: %02u",set[2]); break;
	       				  							}; break;	       				  							  
  case 4:printf(lcd_putc,"\r�y㸻Ľ�� 1");   switch(menu_sb_pos){
	       				  							case 0:printf(lcd_putc,"\n�ac�: %02u",set[0]); break;
	       				  							case 1:printf(lcd_putc,"\nM��y��: %02u",set[1]); break;
	       				  							case 2:printf(lcd_putc,"\nO��o: %02u M��.",set[2]); break;
	       				  							}; break; 
  case 5:printf(lcd_putc,"\r�y㸻Ľ�� 2");   switch(menu_sb_pos){
	       				  							case 0:printf(lcd_putc,"\n�ac�: %02u",set[0]); break;
	       				  							case 1:printf(lcd_putc,"\nM��y��: %02u",set[1]); break;
	       				  							case 2:printf(lcd_putc,"\nO��o: %02u M��.",set[2]); break;
	       				  							}; break; 
  case 7:printf(lcd_putc,"\r�y㸻Ľ�� 1\nླྀ He�.:"); WDayOut(set[0]);  break;
  case 8:printf(lcd_putc,"\r�y㸻Ľ�� 2\nླྀ He�.:"); WDayOut(set[0]);  break; 	       				  								       				  							
	       				  							
 }  
}
void menu_click()
{
int8 m_item;	
menu_lv++;
menu_sb_pos=0;
if (menu_lv>2)menu_lv=0;
lcd_putc('\f');
m_item=menu_lv*3+menu_mn;
 switch (m_item){
	case 0:ds1307_set_date_time(set[0],set[1],set[2],date.dow,time.hrs,time.Min,time.Sec);break; 
	case 3:set[0]=time.hrs; set[1]=time.Min; set[2]=date.dow; break;
	case 6:ds1307_set_date_time(date.day,date.mth,date.year,set[2],set[0],set[1],0);
		   set[0]=date.day; set[1]=date.mth; set[2]=date.year; break;
    case 4:set[0]=Alarm[0].hrs; set[1]=Alarm[0].Min; set[2]=Alarm[0].Win; break;
    case 5:set[0]=Alarm[1].hrs; set[1]=Alarm[1].Min; set[2]=Alarm[1].Win; break;
  	case 7:Alarm[0].hrs=set[0]; Alarm[0].Min=set[1]; Alarm[0].Win=set[2];set[0]=Alarm[0].bweek;SettingWrite(); break;
  	case 8:Alarm[1].hrs=set[0]; Alarm[1].Min=set[1]; Alarm[1].Win=set[2];set[0]=Alarm[1].bweek;SettingWrite();break;	 
  	case 1:Alarm[0].bweek=set[0];SettingWrite(); break;
  	case 2:Alarm[1].bweek=set[0];SettingWrite(); break;  	  	
};
ds1307_get_time(time.hrs,time.min,time.sec);
ds1307_get_date(date.day,date.mth,date.year,date.dow);

  JAct=1;
  menu_view();
}
void menu_change()
{int8 m_item;
 m_item=menu_lv*3+menu_mn;
if (menu_lv==0){
 if (jvh>3) menu_mn++;
 if (jvh<3) menu_mn--;
 if (menu_mn>250) menu_mn=2;
 if (menu_mn>2) menu_mn=0;
 
 } 
 else {
 if (jvh>3 & menu_sb_pos<2) menu_sb_pos++;
 if (jvh<3 & menu_sb_pos>0) menu_sb_pos--;
 if (m_item>6) menu_sb_pos=0;
 }
lcd_putc('\f');	
JAct=1;
menu_view();
//menu_sb_pos=0; 
}
void menu_volume_change()
{int8 m_item;	

 int8 mx[3];
 int8 mn[3];
 m_item=menu_lv*3+menu_mn;
  switch(m_item){
   			// case 3:mx[0]=23; mx[1]=59; mx[2]=120; mn[0]=0; mn[1]=0; mn[2]=0; break;
   			 case 3:mx[0]=23; mx[1]=59; mx[2]=59; mn[0]=0; mn[1]=0; mn[2]=0; break;
   			 case 6:mx[0]=31; mx[1]=12; mx[2]=50; mn[0]=1; mn[1]=1; mn[2]=0; break;
   			 case 4:mx[0]=23; mx[1]=59; mx[2]=200; mn[0]=1; mn[1]=1; mn[2]=0; break;
   			 case 5:mx[0]=23; mx[1]=59; mx[2]=200; mn[0]=1; mn[1]=1; mn[2]=0; break;
   			 case 7:mx[0]=10; mx[1]=0; mx[2]=0; mn[0]=0; mn[1]=0; mn[2]=0; break;
   			 case 8:mx[0]=10; mx[1]=0; mx[2]=0; mn[0]=0; mn[1]=0; mn[2]=0; break;
   			 
  			} 
if (jvv<3) set[menu_sb_pos]++;
if (jvv>3) set[menu_sb_pos]--;
if (set[menu_sb_pos]>mx[menu_sb_pos])set[menu_sb_pos]=mn[menu_sb_pos];
if (set[menu_sb_pos]<mn[menu_sb_pos])set[menu_sb_pos]=mx[menu_sb_pos];
JAct=1;
menu_view();
}


void The_jaction_task()
{ 
  if (JTimer>0) JTimer--;
  if (jvv==3){JTimer=0; jtlevelV=0;};
  if (jvv!=3) if (jtimer==0)
	 {menu_volume_change();jtlevelV++;
	  if (jtlevelV==1)JTimer=8;
      if (jtlevelV>1) if (jvv<2 | jvv>4) JTimer=1;  else JTimer=4; 
      if (jtlevelV>10)jtlevelV=10;};  
  if (jvh==3) jtlevelh=0;
  if (jvh!=3 & jtlevelh==0) {jtlevelh=1; menu_change();};
  if (!input(pin_A2) & !but1)  menu_click();
  but1=!input(pin_A2);
} 

void The_Read_ADC_task()
{ int8 tadc;
  int8 tad;
  tadc= Read_ADC(ADC_READ_ONLY);
  ch=ch^1;
  set_adc_channel(ch);
  tad=3;
  if (tadc>155)tad=4;
  if (tadc==255)tad=5;
  if (tadc<80)tad=2;
  if (tadc==0)tad=1;
  if (ch==0)jvv=tad;
  if (ch==1)jvh=tad;
  delay_us(15);
  Read_ADC(ADC_START_ONLY);
  The_jaction_task();
  
}

void The_read_time_task()
{
  ds1307_get_time(time.hrs,time.min,time.sec);
  restart_wdt();
  menu_view();
  RecLog();
}

void The_date_time_task()
{
  ds1307_get_date(date.day,date.mth,date.year,date.dow);
}
int1 checkInWeek(int8 num)
{int1 res;
res=0;	
if (Alarm[num].bweek==10)res=1;
if (Alarm[num].bweek==date.dow)res=1;	
if (Alarm[num].bweek==8 & date.dow<6)res=1;
if (Alarm[num].bweek==9 & date.dow>5)res=1;
return res;
}
int8 CheckWinAl(int8 num)
{signed int16 AT,T;
 int8 res;
 AT=AlarmToInt16(num);
 T=TimeToInt16();	
 RES=0;
 if (AT==T)Res=10;
 if (T>=AT-Alarm[num].win & T<AT)Res=1;	
 if (!checkInWeek(num))res=0;
  return res;
}
int1 EnRec()
{int1 res;
 res=0;
 if (LogSetting.SHrs==LogSetting.FHrs) res=1;
 if (LogSetting.SHrs>=Time.Hrs & LogSetting.FHrs<=Time.Hrs) res=1;
}
void LogCont()
{
LogCnt++;
if (LogCnt>230)LogCnt=230;
pddf=0;	
}
void RecLog()
{int8 count;
 int16 TimeN;
 TimeN=TimeToInt16();
 	if (EnRec() & (abs(TimeN-LogTime)>=3)){
  		write_eeprom(239,date.day);
  		write_eeprom(240,date.mth);
  		write_eeprom(241,time.hrs);
  		write_eeprom(242,time.min);
  		count=read_EEPROM(243);
  		if (count>238)count=0; 
  		write_eeprom(count,LogCnt);
  		count++;
  		if (count>238)count=0;  		 
  		write_eeprom(243,count);  
  		LogTime=TimeN;	
  		LogCnt=0;		
 		};
 
 
}
void sound_LCDL()
{ 
  if((CheckWinAl(0)+CheckWinAl(1)>9) & sound_en_end==1){sound_time=1000; sound_en_end=0;LCD_Ligth_time=10;}
  if ((CheckWinAl(0)+CheckWinAl(1))==0) {sound_en_end=1;sound_en=1;}
  if((CheckWinAl(0)==1 | CheckWinAl(1)==1) & sound_en==1 & input(ddv)==1 & sound_en_end==1){sound_time=50; sound_en=0;} 
  if(input(ddv)==0 & sound_en_end==1) sound_en=1;  
  if ((CheckWinAl(0)+CheckWinAl(1)>0) & LCD_duty==BRG_Mn  & sound_en_end==1) LCD_Ligth_time=10;
  if (sound_time>0 & JAct==1){sound_time=0;sound_en=0; sound_en_end=0;};
  
  if (pddf==3 & TLog_Rec==0){LogCont(); TLog_Rec=1;};
  if (input(ddv)==0) TLog_Rec=0; 
  if (JAct==1){LCD_Ligth_time=30; JAct=0; };	 
  if (input(ddv)==1) LCD_Ligth_time=10; 
  
  if (sound_time==0) output_bit(zummer,0);
   else { sound_time--; output_toggle(zummer);};
  
  if (LCD_Ligth_time==0 & LCD_duty>BRG_Mn) LCD_duty=LCD_duty-5; 
  if (LCD_Ligth_time>0) {LCD_Ligth_time--;if (LCD_duty<BRG_Mx) LCD_duty=LCD_duty+10;}
  set_pwm1_duty(LCD_duty);
  if (LCD_Ligth_time==0 & LCD_duty==BRG_Mn) {menu_lv=0; menu_mn=0;};
}
void Delay()
{int16 tmr;
 tmr=0;
 while (tmr<1500)
 {
	 tmr++;
	 if (input(ddf)==1 )pddf=3;
	// if (input(ddf)==1 & pddfo==0){pddf++; pddfo=1;};
	// if (input(ddf)==0)pddfo=0;
	// if (pddf>3)pddf=3;
 }
	
}
void SendLogAllPage()
{
int8 re;
fputc(255,PC);
fputc(2,PC);
for(n=0;n<239;n++){
 re=read_eeprom(n);
 if (re>252)re=0;	
 fputc(re,PC);	
 };	 
 fputc(read_eeprom(239),PC);
 fputc(read_eeprom(240),PC);
 fputc(read_eeprom(241),PC);
 fputc(read_eeprom(242),PC);
 fputc(read_eeprom(243),PC);
fputc(254,PC);
 	
}
void SendConnectedMessage()
{
fputc(255,PC);
fputc(0,PC);
fputc(254,PC);	
}
void SendAlarm_1_2()
{
fputc(255,PC);
fputc(4,PC);
 for(n=0;n<2;n++){
  fputc(alarm[n].hrs,PC);
  fputc(alarm[n].min,PC);
  fputc(alarm[n].win,PC);
  fputc(alarm[n].bweek,PC);	
 };	
 fputc(LogSetting.SHrs,PC);
 fputc(LogSetting.FHrs,PC);
fputc(254,PC);	
}
void SetDateTime()
{
ds1307_set_date_time(rs_buff[1],rs_buff[2],rs_buff[3],rs_buff[4],rs_buff[5],rs_buff[6],rs_buff[7]);	
The_read_time_task();
The_date_time_task();
SendConnectedMessage();
}
void SetAlarm_1_2()
{
  alarm[0].hrs=rs_buff[1]; 
  alarm[0].min=rs_buff[2];
  alarm[0].win=rs_buff[3];
  alarm[0].bweek=rs_buff[4];
  alarm[1].hrs=rs_buff[5];
  alarm[1].min=rs_buff[6];
  alarm[1].win=rs_buff[7];
  alarm[1].bweek=rs_buff[8];
  LogSetting.SHrs=rs_buff[9];
  LogSetting.FHrs=rs_buff[10];
  SendConnectedMessage();
  SettingWrite();
}
void LogClear()
{
for(n=0;n<239;n++) write_eeprom(n,0);
write_eeprom(243,0);
SendConnectedMessage();	
}
void rs_read()
{
 switch(rs_buff[0]){
  case 2:SendLogAllPage();break; 
  case 3:SendConnectedMessage();break;
  case 4:SendAlarm_1_2();break;
  case 5:SetDateTime();break;
  case 6:SetAlarm_1_2();break;
  case 7:LogClear();break;
 };	
rs_state=0;	
}
void main()
{  
   LogCnt=0;
   SET_TRIS_C(161);
   setup_adc_ports(AN0_AN1_AN3);
   setup_adc(  ADC_CLOCK_INTERNAL  );
   setup_timer_0(RTCC_INTERNAL|RTCC_DIV_2);
   setup_wdt(WDT_2304MS);
   setup_timer_1(T1_INTERNAL|T1_DIV_BY_2);
   setup_timer_2(T2_DIV_BY_4,124,1);
   setup_ccp1(CCP_PWM);
   setup_ccp2(CCP_PWM);
   set_pwm1_duty(100);
   set_pwm2_duty(0);
   setup_comparator(NC_NC_NC_NC);
   setup_vref(FALSE);
 //  enable_interrupts(INT_AD);
   enable_interrupts(INT_RDA);
  // enable_interrupts(INT_TIMER1);
   enable_interrupts(GLOBAL);
 //  SET_TRIS_B( 0x00 );
   ds1307_init();
   lcd_init();
   output_bit(zummer,0);
   SettingRead();
   pddf=0;
// ADC+++++++++++
   set_adc_channel(ch);
   Read_ADC(ADC_START_ONLY);
   for(n=0;n<4;n++)tmrs[n]=0;
//+++++++++++++++ 

 while (1)
  { 
    Delay();
    for(n=0;n<4;n++)tmrs[n]++;
    if (tmrs[0]>4){The_Read_ADC_task();tmrs[0]=0;};
    if (tmrs[1]>20){The_read_time_task();tmrs[1]=0;};
    if (tmrs[2]>200){The_date_time_task();tmrs[2]=0;};
    if (tmrs[3]>8){sound_LCDL();tmrs[3]=0;};
    if (rs_state==2)rs_read();
    

  };
    
}
