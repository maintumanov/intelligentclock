#include <16F876A.h>
#device *=16
#device adc=8

#FUSES WDT                   	//Watch Dog Timer
#FUSES HS                    	//High speed Osc (> 4mhz for PCM/PCH) (>10mhz for PCD)
#FUSES PUT                   	//Power Up Timer
#FUSES NOPROTECT             	//Code not protected from reading
#FUSES NODEBUG               	//No Debug mode for ICD
#FUSES BROWNOUT              	//Reset when brownout detected
#FUSES NOLVP                 	//No low voltage prgming, B3(PIC16) or B5(PIC18) used for I/O
#FUSES NOCPD                 	//No EE protection
#FUSES NOWRT                 	//Program memory not write protected

#use delay(clock=20000000,RESTART_WDT)
#define POS1   PIN_A0
#define POS2   PIN_A1
#define USB_PWR   PIN_A5
#define Zummer   PIN_B0
#define RS   PIN_B1
#define RW   PIN_B2
#define E   PIN_B3
#define D4   PIN_B4
#define D5   PIN_B5
#define D6   PIN_B6
#define D7   PIN_B7
#define DDV   PIN_C0
#define DDF   PIN_C5
//#define PW220   PIN_C5
#define TX   PIN_C6
#define RX   PIN_C7
#use rs232(baud=9600,parity=N,xmit=TX,rcv=RX,bits=8,stream=PC,restart_wdt)
#use i2c(Master,Fast,sda=PIN_C4,scl=PIN_C3,restart_wdt)
//#use rtos(timer=1,minor_cycle=1ms)
